#! /bin/csh

# Define input variables

set t=mon
set ex=historical
set var=uas
# list of institutions: BCC CCCma CNRM-CERFACS MRI MIROC IPSL NASA-GISS EC-Earth-Consortium
# list of models (source id): BCC-CSM2-MR BCC-ESM1 CNRM-CM6-1 CNRM-ESM2-1 CanESM5 EC-Earth3 GISS-E2-1-G GISS-E2-1-H IPSL-CM6A-LR MIROC6 MRI-ESM2-0 UKESM1-0-LL 

# Use retriever.py to download the data

#python2.6 retriever.py -q -p CMIP6 -var uas -l 1
#python2.6 retriever.py CMIP6 -var uas -t mon -ex historical -l 10
#python2.6 retriever.py CMIP6 -var uas -t mon -ex historical -i BCC

# Run the wget script to download the data
#for now run outside of script

#cd ./data
#echo `pwd`
#chmod +x fetch_*.sh

#./fetch_*.sh -H

#Must make change so the data downloads elsewhere

#Make a historical average from each model across runs

cd ./data/downloads
echo `pwd`
set DIRpath = `pwd`

#foreach insti (BCC CCCma CNRM-CERFACS MRI MIROC IPSL NASA-GISS EC-Earth-Consortium)
#foreach model (MRI-ESM2-0 BCC-CSM2-MR BCC-ESM1)

# Make average of all the runs
	
#	set filelist = `ls  *$model*.nc`
#	echo $filelist
#	set outfile = $DIRpath/average.uas.Amon.$model.historical.185001-201412.nc
#set outfile = $DIRpath/average_uas_Amon_BCC-CSM2-MR_historical_185001-201412.nc

#	ncra -h -O  $filelist $outfile
#	echo $outfile

#end


#regrid for different resolutions


#Creat a multimodel average (doesn't work until all are regridded)

#set filelist1 = `ls *average*.nc`
#echo $filelist1
#set outfile1 = $DIRpath/model_average_uas_Amon_historical_185001-201412.nc

#	ncra $filelist1 $outfile1
#	echo $outfile1

#Make a map

foreach name (uas)
foreach mod (MRI-ESM2-0 BCC-CSM2-MR BCC-ESM1)

	echo $model

	ncl varname=\"{$name}\" model=\"{$mod}\" makemap.ncl
 
end
end

exit
#Upload to monsoon
