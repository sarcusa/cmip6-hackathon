#!/bin/python
# TODO: 
# add in more functionality with CMIP6_CV

# modules

import argparse
import sys,subprocess,os
import json,urllib
import numpy as np
import textwrap

# env variables

data_dir = "./data"
cv_dir = "./CMIP6_CVs"
dict_dir = "./dictionaries"
data_node="https://esgf-node.llnl.gov"
fetch_url=data_node+"/esg-search/wget?"
query_fmt = data_node+"/esg-search/search?sort=true&format=application%2Fsolr%2Bjson"

# argument parsing

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description="Retrieves data from ESGF according to optional parameter values",
    epilog=textwrap.dedent('''\
                           download example: '''+sys.argv[0]+'''-p cmip3 -var tas -r atmos -t mon -ex historical -en run2 -i MOHC
                           query example: '''+sys.argv[0]+''' -q -p cmip3 -r atmos -t mon -ex -en -i -m'''))
parser.add_argument('-q', dest='query', action='store_true', help="query valid parameters sets with output specified by included flags")
parser.add_argument('-l', dest='limit', nargs='?', default=10, type=int, help="number of query outputs")
parser.add_argument('-o', dest='offset', nargs='?', default=0, type=int, help="start number of query outputs")
parser.add_argument('-r', dest='realm', nargs='?', const=True, default=True)
parser.add_argument('-p', dest='project', nargs='?', const=True, default=True)
parser.add_argument('-prod', dest='product', nargs='?', const=True)
parser.add_argument('-m', dest='model', nargs='?', const=True)
parser.add_argument('-var', dest='variable', nargs='?', const=True)
parser.add_argument('-ver', dest='version', nargs='?', const=True)
parser.add_argument('-ex', dest='experiment', nargs='?', const=True, default=True)
parser.add_argument('-t', dest='time_frequency', nargs='?', const=True)
parser.add_argument('-i', dest='institute', nargs='?', const=True)
parser.add_argument('-en', dest='ensemble', nargs='?', const=True, default=True)
parser.add_argument('-in', dest='index_node', nargs='?', const=True)
parser.add_argument('-id', dest='id', nargs='?', const=True)
parser.add_argument('-c', dest='cmor_table', nargs='?', const=True)
args = parser.parse_args()
args_dict = vars(args)

# vocabulary dictionaries

realms = json.load(open(dict_dir+"/realm.json","r"))["realm"] 
projects = json.load(open(dict_dir+"/project.json","r"))["project"] 
time_frequencies = json.load(open(dict_dir+"/time_frequency.json","r"))["time_frequency"] 
institutes = json.load(open(dict_dir+"/institute.json","r"))["institute"] 
products = json.load(open(dict_dir+"/product.json","r"))["product"] 
models = json.load(open(dict_dir+"/model.json","r"))["model"] 
variables = json.load(open(dict_dir+"/variable.json","r"))["variable"] 
versions = json.load(open(dict_dir+"/version.json","r"))["version"] 
cmor_tables = json.load(open(dict_dir+"/cmor_table.json","r"))["cmor_table"] 
index_nodes = json.load(open(dict_dir+"/index_node.json","r"))["index_node"] 
ensembles = json.load(open(dict_dir+"/ensemble.json","r"))["ensemble"] 
eexperiments = json.load(open(dict_dir+"/experiment.json","r"))["experiment"] 


# query

query_url = query_fmt+"&limit="+str(args.limit)+"&offset="+str(args.offset)+"&fields="

if args.query:

    # printed fields in query
    for key,value in args_dict.items():
        if ( key != "query" ):
            if(value == True or isinstance(value,str)): query_url=query_url+key+","

    # specified fields in query
    for key,value in args_dict.items():
        if ( key != "query" ):
	    if isinstance(value,str): query_url=query_url+"&"+key+"="+str(value)
    
    # read query result
    response = urllib.urlopen(query_url)
    response = json.loads(response.read())
    
    if (len(response["response"]["docs"]) == 0): 
        print("no query results"); exit()
      
    # print query result
    print("")
    print(json.dumps(response["response"]["numFound"])+" results found")
    print("Showing results "+str(args.offset)+" through "+str(args.offset+args.limit-1))
    print("")
    for i in range(len(response["response"]["docs"])):
        mylist = []
        variables = ""
	for key,value in response["response"]["docs"][i].items():
	    entry = json.dumps(key)+"="+json.dumps(value)
	    if ((str(key) != "score") and (str(key) != "variable")): 
		mylist.append(entry.replace("\"","").replace("]","").replace("[",""))
            if (str(key) == "variable"): variables = variables+entry
	if (variables != ""): mylist.append(variables.replace("\"","").replace("]","").replace("[",""))    
        print('[%s]' % ', '.join(map(str, mylist)))
        print("")
    exit()

# setup download script

script_name="fetch"

for key,value in args_dict.items():
    if ( key != "query" ):
        if isinstance(value,str):
            fetch_url=fetch_url+"&"+key+"="+str(value)
	    script_name=script_name+"_"+str(value)

script_name=script_name+".sh"    

# check dictionaries for valid entries

'''
for key,value in args_dict.items():
    if ( key != "query" ) and ( key != "id" ):
        if isinstance(value,str):
	    dict_name = key+"s"+".keys()"
	    if value not in dict_name:
	        print("invalid "+key+" entry"); exit()
'''

# get download script

fetch_cmd = "wget -O "+script_name+" "+fetch_url
process = subprocess.Popen(fetch_cmd.split(), stdout=subprocess.PIPE)
output,error = process.communicate()

# check for empty script

if "bash" not in open(script_name,'r').readline():
    print("Empty script downloaded: This is an invalid parameter set")
    rm_cmd = "rm -f ./"+script_name
    subprocess.Popen(rm_cmd.split(), stdout=subprocess.PIPE)
    exit()

# make data dir for download

mkdir_cmd = "mkdir -p "+data_dir
mv_cmd = "mv "+script_name+" "+data_dir
os.system(mkdir_cmd+"; "+mv_cmd)
os.chdir(data_dir)

# run download script

if False:
    import pexpect
    dl_cmd = "bash ./"+script_name+" -H"
    child = pexpect.spawn(dl_cmd)
    child.expect("Enter your openid :")
    child.sendline("doesnt matter")
    child.expect("Enter password :")
    child.sendline("doesnt matter")
    child.interact()
