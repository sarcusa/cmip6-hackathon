'reinit'
pathref="/FoudreD0/data/ccctus/prog/interann/prog/clim/var.iann.ja.gs" 
*----------------------* 
*title1a = 'VAR(P[JA])'; 
* title1a = 'VAR(P)'; 
*----------------------* 
case=1;* JA  2002 Livneh
*case=2;* JA  1999 Livneh 
*case=2;* 
*----------------------* 
thck1 = 12; 
thck2 = 6; 
*----------------------* 
*----------------------* 
 if(case=1) 
    ncfile1 = 'tmp.avg.nc'
    pngfile = './uas.cmpi6.avg.png'
    title1a = 'CMIP6(UAS) avegerage'; 

    initime = 00Z01Jan1963; endtime = 00Z01Jan1963; 
    initime = 00Z01Jun1964; endtime = 00Z01Aug1964; 
    initime = 00Z01Jun1965; endtime = 00Z01Aug1965; 
    initime = 00Z01Jun1966; endtime = 00Z01Aug1966; 

    initime = 00Z01Jun1963; endtime = 00Z01Aug1967; 
    
*   initime = 00Z01Jan1999; 
    title1b = '   ';endif 
*----------------------* 
* if(case=2) 
*   title1b = '1993: Wet';endif 
*----------------------* 
*--------------------------------------------------------------------------------* 
*--------------------------------------------------------------------------------* 
*epsfile = 'pr.var.var.'%models%'.'%substr(title1b,2,math_strlen(title1b)-2)%'.ja.eps'
* say substr(title1b,2,math_strlen(title1b)-2) 
*----------------------* 
*'sdfopen 'ncfile''
*'open 'ncfile''

 'sdfopen 'ncfile1''

 'q gxinfo'
  say result
  xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
  xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
  dx=(xmax-xmin)/2;dy=(ymax-ymin)/2
*--------------------------
*-NE 
  xl=xmin+0.7;xr=xl+dx*0.85;
  yt=ymax-0.7;yb=yt-dy*0.70;
*-N. America
  xl=xmin+0.7;xr=xl+dx*1.04;
  xl=xmin+0.7;xr=xl+dx*1.01;
  yt=ymax-0.7;yb=yt-dy*1.25;

 'set parea 'xl' 'xr' 'yb' 'yt''
*---------
'set grads off'
'set frame off'
'set xlab off'
'set ylab off'
*---------------* 
* the NAM region 
*'set lat 25 40'
*'set lon -120 -100'
* US region 
*'set lat 15 50'
*'set lon -125 -80'
* East Coast region 
*'set lat 25 48.5'

*-'set lat 26 48.5'
*-'set lon -88 -66'
*'set lat 21 60.0'

* 'set lat 22 58.0'
* 'set lon -130 -66'

*   var   * 
ntime=12*116
ntime=140
*ntime=12*20

  scale = 86400.0;* mm/day
  scale = 1.0;* mm/day

 'set time 'initime'';'q dim';
  xline=sublin(result,5);itime=subwrd(xline,9);
 'set time 'endtime'';'q dim';
  xline=sublin(result,5);etime=subwrd(xline,9);

*'define xvar = smth9('scale'*( data.1 + data.2 ) )'

*-'define xvar = ave(pdsi,t='itime',t='etime')'

*-'define xvar = ave(cc,t='itime',t='etime')'
*-'define xvar = 'scale' * ave(cc.2-cc.1,t='itime',t='etime')'

*'define xmean = 'scale' * ave(cc,t=1,t=84)'
*'define xvar = 'scale' * ave(cc,t='itime',t='etime') - 'xmean''

'set t 1' 
'define xvar = 'scale' * uas'

*'define xvar = smth9(smth9(smth9('scale' * prec(t='itime') ))) - xmean'
*'define xvar = smth9(smth9(smth9('scale' * pdsi(t='itime') )))'
*'define xvar = xtmp'

'set gxout shaded'
'set grid off'

'set ylopts 1 3 0.15'
'set xlopts 1 3 0.15'

*'set ylint 15'
*'redblue'
 'pdsi_brown_green'

**'set clevs 1.0 2.0 3.0 4.0 5.0'
** to define color check: 
**  olor 1 10 -kind red->white -sample
** source:  http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script/color.gs?lang=en 
**  color 1 5 -kind skyblue->darkgreen -sample  
*ccols= 16 17 18 19 20 21 22 23 24 25 26 27

  clevs = '-2.5 -2 -1.5 -1 -0.5 0 0.5 1 1.5 2 2.5'
*'set clevs  'clevs''
*'set ccols  21 22 23 24 25 26 31 32 33 35 36 37'

*'set ccols  21 22 23 24 25 26 31 32 33 34 35 36 37'
*'set ccols  21 22 23 24 25 0 0 31 32 33 34 35'

'd xvar'

*'cbarv.big.local1  'xr+0.07' 'yb+0.00' 0 '0.24' '0.24' '
'cbarv.big.local1  'xr+0.40' 'yb+2.00' 0 '0.24' '0.24' '

*----------
*
 'set gxout contour'
  clevs = '0  1 2'
 'set clevs 'clevs'' 
* 'set line 1 1 'thck2'' 
 'set ccolor 1';'set clab off';'set cthick 'thck2''
 
 'd xvar'
  clevs = '-2 -1 0'
 'set ccolor 1';'set clab off';'set cthick 'thck2''
 'd -1*xvar'
*

*- mask values over Canada and Mexico------
*-'remove_mx_can.gs' 
*-------

*-'basemap O 0 1 L' 

* 'draw map'
*--
'set strsiz 0.14 0.14'
'set string 1 l 'thck1''
'draw string 'xl+0.05' 'yt-0.80' 'title1a''
'set strsiz 0.13 0.13'
'draw string 'xl+0.25' 'yb+0.15' 'title1b''
* 
*'set string 1 l 4'
*
* x-y label 
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

xlab=on
ylab=on
*if(xlab=on);xvlf=0;xvrg=180;xint=60;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(xlab=on);xvlf=00;xvrg=360;xint=40;
            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(ylab=on);yvb=-80.0;yvt=80.0;yint=20;
            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif
* 
*------------------------

'set mpdraw on';
*'set mpdset hires'
 'set mpdset lowres';* Global contourd 
*'set mpdset nmap'
*'set mpdset mres'; *states limits 
'set map 1 1 'thck1'';
*'set map 1 1 'thck1'';
* 'set map 1 1 3';
*'set mpt 1 15 1 6';
'draw map'
*--this add contours--
'set mpt 1 off'
'set gxout contour'
'set line 1'
'set shpopts -1';* use -1 for unfilled poligons 
*'draw shp /data/cmc542/shp/CAN/province' 
*'draw shp /data/cmc542/shp/CAN2/canada' 
'set line 1 1 'thck2'' 
*--'draw shp /data/cmc542/shp/work/CAN_adm0';* US-Canada border 

*'remove_mx_can.gs' 

*'set mpdset lowres'; 
*'set map 37 1 'thck1'';
*'draw map'
*-----------

'myframe 1 1 'thck2''

*-----------
*'set strsiz 0.10 0.12'
*'set string 1 l'
*'draw string 0.5 2.40 'pathref''
*------------------------* 
if (1=1) 
  xres=2000*0.85
  yres=2000*1.10
  'printim 'pngfile' x'xres' y'yres' white'
*'enable print x.gm'
*'print'
*'disable print'
*'!gxeps -c -i x.gm -o x.eps'
*'!convert -density 200 x.eps 'pngfile''
*'!mv x.eps 'epsfile''
endif 
*-------------------------* 
'quit'



*---------------------------
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
thck1=12;
thck2=6;
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        xabs=math_abs(xx.it)
        say xreal 
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else; 
                xtmp = 360-xabs
                xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if(xreal<=180) 
            xxlb.it=xabs%"`aO`nE";
           else 
              xtmp = 360-xabs
              xxlb.it=xtmp%"`aO`nW";
           endif 
           if(xreal=0|xreal=360);xxlb.it="0`aO`n";endif

        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
*        'set strsiz 0.14'
         'set strsiz 0.12 0.12'
         'set string 1 c 'thck1''
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
thck1=12
thck2=6
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.14'
         'set strsiz 0.12 0.12'
         'set string 1 r 'thck1''
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------

