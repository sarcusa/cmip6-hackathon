#! /bin/csh 
#--
#--
set aveflag = "no"
set aveflag = "yes" 
#--
set model = 'CNRM-ESM2-1' 
set model = 'CNRM-ESM2-1_piClim' 

#--
#-- average files 
#--
if ( $aveflag == "yes" ) then  

   ls  /data2/cmc542/scratch/2019/mip-tools/data/*${model}*  > toave.txt 

   set files = `cat toave.txt` 
#  set files = `cat toave.2.txt` 

#--Method 1 with SMALL list of files 
#  ncrcat --ovr  -h $files tmp.all.nc
#  ncwa --ovr -a time tmp.all.nc tmp.avg.nc
#--Method 2 with LARGE list of files 
   set listfile = "nwlist.txt" 
   rm -rf $listfile
   foreach file ($files)
      echo $file 
      set outfile = $file:r.avg.nc 
      echo $outfile 
#-----average individual files 
      ncra --ovr -F -d time,1,,1 $file $outfile
      echo $outfile >> $listfile
   end
   set nwlist = `cat $listfile` 
#--merge list of files
   ncrcat --ovr -h $nwlist tmp.all.nc
#--average tmp.all.nc file 
   ncra --ovr -F -d time,1,,1  tmp.all.nc tmp.avg.nc

endif 

#-exit 

#--
#-- plot single file 
#-- 
# usage:
#      
# grads -bpc "mkplt.gs tmp.avg.nc CNRM.png uas" 
#          mkplt.gs, grads script 
#          mp.avg.nc, input file
#          CNRM.png, pngfile output
#          uas, variable name  of input file 
# 
grads -bpc "mkplt.gs tmp.avg.nc CNRM.png uas" 

exit 

foreach file ($files)
  echo $file 
  set outfile = $file:r.avg.nc 
  echo $outfile 
# ncwa --ovr -a time $file $outfile
end

#ncks -A data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp.nc
#ncks -A data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp.nc
#ncks -A data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp.nc

cp data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp1.nc
cp data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp2.nc
cp data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  tmp3.nc

ncks -O --mk_rec_dmn time tmp1.nc
ncks -O --mk_rec_dmn time tmp2.nc
ncks -O --mk_rec_dmn time tmp3.nc

ncks -O --mk_rec_dmn time tmin_cfsv2.nc $xpath/tmin_Aday_NRCC_LatLon_cfsv2_${yyyy}_dy.nc 

ncrcat -h tmp1.nc tmp2.nc tmp3.nc tmp.all.nc


exit 
ncrcat -h  data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  data/uas_Amon_CNRM-ESM2-1_piClim-2xdust_r1i1p1f2_gr_185001-187912.avg.nc tmp.nc

ncrcat -h  data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.avg.nc  data/uas_Amon_CNRM-ESM2-1_piClim-2xdust_r1i1p1f2_gr_185001-187912.avg.nc tmp.nc

exit 
rm -rf t2m.veg.nc
ncks --ovr -v uas data/uas_Amon_CNRM-ESM2-1_piClim-2xDMS_r1i1p1f2_gr_185001-187912.nc  t2m.veg.nc
ncwa --ovr -a time t2m.veg.nc t2m.veg.avg.nc


